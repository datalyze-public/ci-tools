# syntax=docker/dockerfile:1.5-labs
ARG SHLIBS_VERSION="v1"
ARG DOCKER_VERSION="27"
FROM registry.gitlab.com/datalyze-public/shlibs:${SHLIBS_VERSION} AS shlibs
FROM docker:${DOCKER_VERSION} AS builder

ARG TARGETOS
ARG TARGETARCH
ARG TARGETPLATFORM
RUN echo "I am building for ${TARGETPLATFORM} with ${TARGETOS} and ${TARGETARCH}"

RUN apk add --no-cache curl~=8 make~=4 bash~=5 git~=2 gcompat~=1

RUN URL="https://storage.googleapis.com/skaffold/releases/latest/skaffold-${TARGETOS}-${TARGETARCH}" && \
    echo "Downloading skaffold from: ${URL}" && \
    wget --quiet --no-verbose --output-document skaffold "${URL}" && \
    install skaffold /usr/local/bin/

# ARG ACORN_VERSION="v0.9.1"
# RUN URL="https://github.com/acorn-io/runtime/releases/download/${ACORN_VERSION}/acorn-${ACORN_VERSION}-${TARGETOS:-linux}-${TARGETARCH:-amd64}.tar.gz" && \
#     echo "Downloading acorn from: ${URL}" && \
#     wget --quiet --no-verbose --output-document acorn.tar.gz "${URL}" && \
#     tar -xvf acorn.tar.gz && \
#     chmod +x acorn && \
#     mv acorn /usr/local/bin/

# ARG CRANE_VERSION="v0.19.1"
# RUN export TMP_DIR="/tmp/crane/" && \
#     mkdir -p ${TMP_DIR} && \
#     cd ${TMP_DIR} && \
#     case ${TARGETARCH} in \
#         "amd64")  ARCH="x86_64" ;; \
#         "arm64")  ARCH="arm64" ;; \
#         *)  ARCH="x86_64" ;; \
#     esac && \
#     case ${TARGETOS} in \
#         "linux")  OS="Linux" ;; \
#         "darwin") OS="Darwin" ;; \
#         *)  OS="Linux" ;; \
#     esac && \
#     URL="https://github.com/google/go-containerregistry/releases/download/${CRANE_VERSION}/go-containerregistry_${OS}_${ARCH}.tar.gz" && \
#     echo "TARGETOS: ${TARGETOS} > OS: ${OS}, TARGETARCH: ${TARGETARCH} > ARCH: ${ARCH}" && \
#     echo "Downloading crane from: ${URL}" && \
#     wget "${URL}" --quiet --no-verbose --output-document "${TMP_DIR}/app.tar.gz" && \
#     tar -xf "${TMP_DIR}/app.tar.gz" && \
#     chmod +x crane && \
#     mv crane /usr/local/bin && \
#     rm -r /tmp/crane

# RUN URL="https://dl.k8s.io/release/$(wget -qO- https://dl.k8s.io/release/stable.txt)/bin/${TARGETOS}/${TARGETARCH}/kubectl" && \
#     echo "Downloading kubectl from: ${URL}" && \
#     wget --quiet --no-verbose --output-document /usr/local/bin/kubectl "${URL}" && \
#     chmod +x /usr/local/bin/kubectl

# ARG JUST_VERSION="1.36.0"
# RUN export TMP_DIR="/tmp/just/" && \
#     mkdir -p ${TMP_DIR} && \
#     case ${TARGETARCH} in \
#         "amd64")  ARCH="x86_64" ;; \
#         "arm64")  ARCH="aarch64" ;; \
#         *)  ARCH="x86_64" ;; \
#     esac && \
#     case ${TARGETOS} in \
#         "linux")  OS="linux" ;; \
#         "darwin") OS="darwin" ;; \
#         *)  OS="linux" ;; \
#     esac && \
#     URL="https://github.com/casey/just/releases/download/${JUST_VERSION}/just-${JUST_VERSION}-${ARCH}-unknown-${OS}-musl.tar.gz" && \
#     echo "Downloading just from: ${URL}" && \
#     wget --quiet --no-verbose --output-document "${TMP_DIR}/app.tar.gz" "${URL}" && \
#     tar -xvf "${TMP_DIR}/app.tar.gz" && \
#     chmod +x just && \
#     mv just /usr/local/bin/ && \
#     rm -r ${TMP_DIR}

# ARG GRYPE_VERSION="0.78.0"
# RUN export TMP_DIR="/tmp/grype/" && \
#     mkdir -p ${TMP_DIR} && \
#     URL="https://github.com/anchore/grype/releases/download/v${GRYPE_VERSION}/grype_${GRYPE_VERSION}_${TARGETOS:-linux}_${TARGETARCH:-amd64}.tar.gz" && \
#     echo "Downloading grype from: ${URL}" && \
#     wget --quiet --no-verbose --output-document "${TMP_DIR}/app.tar.gz" "${URL}" && \
#     tar -xvf "${TMP_DIR}/app.tar.gz" && \
#     chmod +x grype && \
#     mv grype /usr/local/bin/ && \
#     rm -r ${TMP_DIR}

ARG MANIFEST_TOOL_VERSION="2.1.6"
RUN export TMP_DIR="/tmp/manifest_tool/" && \
    mkdir -p ${TMP_DIR} && \
    URL="https://github.com/estesp/manifest-tool/releases/download/v${MANIFEST_TOOL_VERSION}/binaries-manifest-tool-${MANIFEST_TOOL_VERSION}.tar.gz" && \
    echo "Downloading manifest tool from: ${URL}" && \
    wget --quiet --no-verbose --output-document "${TMP_DIR}/app.tar.gz" "${URL}" && \
    tar -xvf "${TMP_DIR}/app.tar.gz" && \
    mv manifest-tool-${TARGETOS}-${TARGETARCH} manifest-tool && \
    chmod +x manifest-tool && \
    mv manifest-tool /usr/local/bin/ && \
    rm -r ${TMP_DIR}

ARG TRIVY_VERSION="0.52.1"
RUN export TMP_DIR="/tmp/trivy/" && \
    mkdir -p ${TMP_DIR} && \
    case ${TARGETARCH} in \
        "amd64")  ARCH="64bit" ;; \
        "arm64")  ARCH="ARM64" ;; \
        *)  ARCH="64bit" ;; \
    esac && \
    case ${TARGETOS} in \
        "linux")  OS="Linux" ;; \
        "darwin") OS="Darwin" ;; \
        *)  OS="Linux" ;; \
    esac && \
    URL="https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_${OS}-${ARCH}.tar.gz" && \
    echo "Downloading trivy from: ${URL}" && \
    wget --quiet --no-verbose --output-document "${TMP_DIR}/app.tar.gz" "${URL}" && \
    tar -xvf "${TMP_DIR}/app.tar.gz" && \
    chmod +x trivy && \
    mv trivy /usr/local/bin/ && \
    rm -r ${TMP_DIR}

ARG GITVERSION_VERSION="6.0.2"
RUN export TMP_DIR="/tmp/gitversion/" && \
    mkdir -p ${TMP_DIR} && \
    case ${TARGETARCH} in \
        "amd64")  ARCH="x64" ;; \
        "arm64")  ARCH="arm64" ;; \
        *)  ARCH="x64" ;; \
    esac && \
    case ${TARGETOS} in \
        "linux")  OS="linux" ;; \
        "darwin") OS="darwin" ;; \
        *)  OS="linux" ;; \
    esac && \
    URL="https://github.com/GitTools/GitVersion/releases/download/${GITVERSION_VERSION}/gitversion-${OS}-${ARCH}-${GITVERSION_VERSION}.tar.gz" && \
    echo "Downloading gitversion from: ${URL}" && \
    wget --quiet --no-verbose --output-document "${TMP_DIR}/app.tar.gz" "${URL}" && \
    tar -xvf "${TMP_DIR}/app.tar.gz" && \
    chmod +x gitversion && \
    mv gitversion /usr/local/bin/ && \
    rm -r ${TMP_DIR}

# ARG SOPS_VERSION="3.9.1"
# RUN export TMP_DIR="/tmp/sops/" && \
#     mkdir -p ${TMP_DIR} && \
#     case ${TARGETARCH} in \
#         "amd64")  ARCH="amd64" ;; \
#         "arm64")  ARCH="arm64" ;; \
#         *)  ARCH="amd64" ;; \
#     esac && \
#     case ${TARGETOS} in \
#         "linux")  OS="linux" ;; \
#         "darwin") OS="darwin" ;; \
#         *)  OS="linux" ;; \
#     esac && \
#     URL="https://github.com/getsops/sops/releases/download/v${SOPS_VERSION}/sops-v${SOPS_VERSION}.${OS}.${ARCH}" && \
#     echo "Downloading sops from: ${URL}" && \
#     wget --quiet --no-verbose --output-document "${TMP_DIR}/sops" "${URL}" && \
#     chmod +x "${TMP_DIR}/sops" && \
#     mv "${TMP_DIR}/sops" /usr/local/bin/ && \
#     rm -r ${TMP_DIR}

FROM docker:${DOCKER_VERSION}

ARG TARGETOS
ARG TARGETARCH
ARG TARGETPLATFORM
ARG BUILD_DATE
ARG BUILD_VERSION
ARG DESCRIPTION
ARG IMAGE_NAME
ARG URL
ARG VSC_URL
ARG VENDOR

ENV SHLIBS_LOGGING_LEVEL="info" \
    SHLIBS_LOGGING_MODE="echo" \
    SHLIBS_LOGGING_STATS="0" \
    PATH=$PATH:/root/.local/bin

RUN apk upgrade && \
    apk add --no-cache curl~=8 make~=4 bash~=5 git~=2 gcompat~=1 npm~=10 jq~=1 yq~=4 helm~=3 openssh-server~=9 nano~=8 rsync~=3 age~=1 coreutils~=9 sops~=3 \
        python3~=3 py3-pip~=24 pipx~=1 uv~=0 opentofu~=1 flux~=2 sed~=4 \
        crane~=0 just~=1 kubectl~=1 grype~=0 kustomize~=5 && \
    npm install --global commit-and-tag-version@^12 && \
    adduser -g 1000 user_1000 --disabled-password && \
    adduser -g 1001 user_1001 --disabled-password && \
    adduser -g 1002 user_1002 --disabled-password
    RUN mkdir -p /root/.ssh && chmod 0700 /root/.ssh && \
    mkdir -p /root/.config/sops/age && chmod 0700 /root/.config/sops/age && \
    mkdir -p /home/user_1000/.ssh && chmod 0700 /home/user_1000/.ssh && \
    mkdir -p /home/user_1001/.ssh && chmod 0700 /home/user_1001/.ssh && \
    mkdir -p /home/user_1002/.ssh && chmod 0700 /home/user_1002/.ssh && \
    mkdir -p /home/user_1000/.config/sops/age && chmod 0700 /home/user_1000/.config/sops/age && \
    mkdir -p /home/user_1001/.config/sops/age && chmod 0700 /home/user_1001/.config/sops/age && \
    mkdir -p /home/user_1002/.config/sops/age && chmod 0700 /home/user_1002/.config/sops/age

COPY --from=builder /usr/local/bin/ /usr/local/bin
COPY --from=shlibs /usr/local/bin/shlibs /usr/local/bin/shlibs
COPY ./bin /usr/local/bin

# download latest commit to force alwas uv tool install
ADD "https://gitlab.com/api/v4/projects/66583315/repository/commits?per_page=1" /usr/local/share/pydops-last-commit
RUN uv tool install git+https://gitlab.com/datalyze-public/pydops

LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.build-date="${BUILD_DATE}"
LABEL org.label-schema.name="${IMAGE_NAME}"
LABEL org.label-schema.version="${BUILD_VERSION}"
LABEL org.label-schema.description="${DESCRIPTION}"
LABEL org.label-schema.url="${URL}"
LABEL org.label-schema.vcs-url="${VSC_URL}"
LABEL org.label-schema.vendor="${VENDOR}"
LABEL org.label-schema.targetos="${TARGETOS}"
LABEL org.label-schema.targetarch="${TARGETARCH}"
LABEL org.label-schema.targetplatform="${TARGETPLATFORM}"