# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## 1.0.0 (2024-02-06)


### Features

* add dockerfile ([e1646c0](https://gitlab.com/datalyze-public/ci-tools/commit/e1646c0b2f68093bf7c2190c0081d18ceb9c6ae6))

## 1.1.0 (2024-02-06)


### Features

* add dockerfile ([e1646c0](https://gitlab.com/datalyze-public/ci-tools/commit/e1646c0b2f68093bf7c2190c0081d18ceb9c6ae6))
